// Fect data for job list
fetch("jobs-data.json")
.then(function(response){
    return response.json();
})

.then(function(datas) {
    let placeholder = document.querySelector("#data-output");
    let out = "";

    for (let data of datas) {
       out +=`
            <div class="col-md-12 shadow rounded px-4 py-3 mb-3">
                <div class="row">
                    <div class="col-lg-5 col-md-12 col-sm-12">
                        <p class="fs-5 fw-bold">${data.position}</p>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <p class="text-primary"><i class="fa-solid fa-location-dot mx-2 text-secondary"></i> ${data.location}</p>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <a href="#" class="btn-blue text-decoration-none py-2 px-5">
                            <span class="text-white">Apply</span>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-sm-6">
                        <p class="fw-light">Requirements:</p>
                    </div>
                    <div class="col-lg-10 col-sm-6">
                        <p class="mb-0 text-secondary">${data.requirements.a}</p>
                        <p class="text-secondary">${data.requirements.b}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-sm-6">
                        <p class="fw-light">Salary:</p>
                    </div>
                    <div class="col-lg-10 col-sm-6">
                        <p class="mb-0 text-secondary">${data.salary}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <p class="text-secondary">${data.posted}</p>
                    </div>
                </div>
            </div>
        </div>
       `;
        
    }

    placeholder.innerHTML = out;
    
})

// Fetch job location
fetch("locations-data.json")
.then(function(response){
    return response.json();
})

.then(function(locations) {
    let placeholder = document.querySelector("#data-loc");
    let loc_output = "";

    for (let location of locations) {
       loc_output +=`
       <li class="list-group-item border border-0 location">
            <a href="#" class="text-secondary text-decoration-none location">${location.city}</a>
        </li>
       `;
        
    }

    placeholder.innerHTML = loc_output;
    
})